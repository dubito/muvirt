#!/bin/sh
set -e

cat > muvirt-lede/feeds.conf <<EOF
src-link muvirt $(readlink -f muvirt-feed)
src-git packages https://git.lede-project.org/feed/packages.git
src-git luci https://git.lede-project.org/project/luci.git
src-git openwisp https://github.com/openwisp/openwisp-config.git
EOF

DOWNLOAD_DIR=$(readlink -f downloads)

cd muvirt-lede
./scripts/feeds clean
./scripts/feeds update -a
./scripts/feeds install luci
./scripts/feeds install dosfstools
./scripts/feeds install muvirt
./scripts/feeds install netkit-telnet
./scripts/feeds install lvm2
./scripts/feeds install tmux
./scripts/feeds install ethtool
./scripts/feeds install -p openwisp openwisp-config-openssl
./scripts/feeds install -p muvirt qemu-nbd
./scripts/feeds install -p muvirt qemu-img
cp ../traverse_muvirt_defconfig .config
echo "CONFIG_DOWNLOAD_FOLDER=\"$DOWNLOAD_DIR\"" >> .config
make defconfig

grep "CONFIG_PACKAGE_muvirt=y" .config || (echo "muvirt not selected" && exit 1)
grep "CONFIG_PACKAGE_luci=y" .config || (echo "LuCI not selected" && exit 1)
grep "CONFIG_PACKAGE_lvm2=y" .config || (echo "LVM not selected" && exit 1)
grep "CONFIG_PACKAGE_tmux=y" .config || (echo "tmux not selected" && exit 1)
grep "CONFIG_PACKAGE_qemu-img" .config || (echo "qemu-img not selected" && exit 1)
grep "CONFIG_PACKAGE_qemu-nbd" .config || (echo "qemu-nbd not selected" && exit 1)

echo "Building with `nproc` cores"
make -j`nproc`
