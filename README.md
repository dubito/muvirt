# μVirt - really small virtualisation

μVirt (aka "micro"Virt or muVirt) is a small virtualisation host built on top of OpenWRT/LEDE, primarily
designed to host simple virtual network functions (VNF) on [universal CPE (uCPE)](https://www.sdxcentral.com/articles/contributed/understanding-use-universal-cpe/2017/07/)
boxes.

The current iteration works on 64-bit ARM hardware and virtual machines compliant
with the [Linaro VM System Specification](https://www.linaro.org/blog/vm-system-specification-v2-arm-processors/) (i.e VM images that use UEFI to boot)

## What μVirt is
- Small - the system image fits inside the NAND or QSPI flash (<64MB), allowing bulk storage (NVMe Flash, SATA and/or RAID) to be dedicated to virtual machines
- Integrates with OpenWRT UCI, configuration in /etc/config/virt, VMs bound to OpenWRT bridge network, can use OpenWRT services such as DHCP or Firewall.


## What μVirt is not
- A replacement for OpenStack, oVirt, Proxmox or vSphere

Think ESXi vs vSphere, HyperV vs SCCM. For example, μVirt hosts have local storage only.

- Production ready - this is currently a tech demonstration. Use at own risk.

- Only serial consoles to the VM are supported, no VNC/SPICE support.

VMs compliant to the Linaro VM specification should have working consoles on ttyAMA0 (as do 'real' ARM servers/those complying with SBAS).

## Current supported hardware:
- Traverse LS1043 Family

In theory, any virtualisation capable Aarch64 OpenWRT host should work, this means any CPU with GICv2 or later interrupt controller.
Use on other KVM-enabled platforms (x86, POWER) is possible but not planned at this stage.

At this time, only 64-bit hosts and VMs are supported (no Aarch32 support).

## TODO:
- Integration with OpenWISPv2 and other management systems (i.e TR-069)
- Backup methods
- OpenvSwitch and OpenvSwitch-DPDK integration
- Examine if using libvirt is worth it (we currently execute qemu-system-aarch64 directly)
- Examine methods for cloud-init configuration (9p, DHCP)
- Implement friendlier methods of VM shutdown and reset (```/etc/init.d/muvirt.<vnmame>``` stop causes a hard shutdown)

## Build notes:
This repository has two submodules (muvirt-lede and muvirt-feed), to obtain the μVirt sources do a
recursive clone:
```
git clone --recursive https://gitlab.com/muvirt/muvirt/
```

To build with the default configuration:
```
./build.sh
```

## Configuration example:
Here, LVM is used as the storage backend, and a LEDE VM is bridged to two outside interfaces.
```
# cat /etc/config/virt
config disk 'lededisk'
        option path '/dev/vg1/lvol0'
        option type 'virtio-blk'

config vm 'lede'
        option type 'aarch64'
        option cpu 'host'
        option memory 128
        option numprocs 1
        option telnet 4446 # This is the port qemu will spawn the serial console telnet on
        option machine 'virt,gic_version=2'
        list mac '52:54:00:F4:A2:BD 52:54:00:CA:5A:A4'
        list disks 'lededisk'
        list network 'privatelan privatewan'
        option enable 1

# cat /etc/config/network
config interface 'privatelan'
        option type 'bridge'
        option ifname 'eth2'
        option proto 'none'

config interface 'privatewan'
        option type 'bridge'
        option ifname 'eth3'
        option proto 'none'
```

## Adding a VM
TODO: Add a `muvirt-deploy` command 
```
lvcreate -L 20GiB --name <vmname> vg1
dd if=vmname.img of=/dev/vg1/<vmname> # or qemu-img convert -O vmname.qcow2 /dev/vg1/<vmname>
vi /etc/config/virt
ln -s /etc/init.d/muvirt /etc/init.d/muvirt.<vmname>
/etc/init.d/muvirt.<vmname> enable
/etc/init.d/muvirt.<vmname> start
```

On boot, /etc/init.d/muvirt will create symlinks for any VMs that are enabled (option enable 1). This means VMs will be set up correctly after a sysupgrade.

## Accessing VM consoles
Use `muvirt-console <vmname>` to spawn a telnet session to the child VM
Use the telnet escape sequence ( ```]``` and type ```quit``` ) to leave the session

_NOTE_: Do not attempt to use muvirt-console over the host's serial terminal, ```muvirt-console```
uses tmux which doesn't like /dev/console.

## Where to get virtual machine images
- Ubuntu
    * [Ubuntu Cloud Images](http://cloud-images.ubuntu.com), such as zesty-server-cloudimage-arm64. These use cloud-init by default.
- Fedora
    * [Traverse Fedora Image Build Script](https://gitlab.com/matt_traverse/build-fedora-aarch64-efi)
    * [Fedora Alternative Architectures](https://alt.fedoraproject.org/alt/)
- Debian
    * [Traverse Debian Image Build Script](https://gitlab.com/matt_traverse/build-debian-aarch64-efi)
- OpenWRT
	* [Traverse OpenWRT ARMv8-UEFI tree](https://gitlab.com/muvirt/openwrt-armvm-uefi) (upstream pending)
- FreeBSD
	* [FreeBSD 11.1](http://ftp.freebsd.org/pub/FreeBSD/releases/VM-IMAGES/11.1-RELEASE/aarch64/Latest/)
	(requires an older QEMU_EFI.fd, see [https://wiki.freebsd.org/arm64/QEMU](FreeBSD Wiki))
- OpenBSD
	* [Notes on OpenBSD](https://gitlab.com/traversetech/muvirt/wikis/OpenBSD)
	(like FreeBSD, requires an older QEMU_EFI.fd. Currently not stable under QEMU)
